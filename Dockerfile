


# -------------------- BUILD --------------------



# Use node image
FROM node:18-alpine AS build

# Set working dir inside base docker image
WORKDIR /usr/src/nodejs-final-project

# COPY package.json
COPY package.json ./

# Install project dependencies
RUN yarn install

# Copy configuration files
COPY . .

# Generate Prisma client files
RUN yarn prisma generate

# Build our nestjs
RUN yarn build



# -------------------- PRODUCTION --------------------



FROM node:18-alpine

WORKDIR /usr/src/nodejs-final-project

COPY --from=build /usr/src/nodejs-final-project/dist ./dist

RUN mkdir -p /usr/src/nodejs-final-project/public/img

COPY package.json ./

RUN yarn install --only=production

COPY --from=build /usr/src/nodejs-final-project/node_modules/.prisma/client  ./node_modules/.prisma/client

RUN rm package.json

EXPOSE 8082

CMD ["node", "dist/main.js"]

# docker build . -t img-nodejs-final-project

# docker run -d -e DATABASE_URL=mysql://freedb_hoanglam08691:bJ%40ZXyc5DsF%3F5p%25@sql.freedb.tech:3306/freedb_nodejs_final_project?schema=public -e KEY=FLASH -p 4001:8082 --name cons-nodejs-final-project img-nodejs-final-project
