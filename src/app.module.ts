import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { UserModule } from './user/user.module';
import { ConfigModule } from '@nestjs/config';
import { JwtStrategy } from './strategy/jwt.strategy';
import { WorkTypeModule } from './work-type/work-type.module';
import { WorkTypeDetailModule } from './work-type-detail/work-type-detail.module';
import { WorkModule } from './work/work.module';
import { MulterModule } from '@nestjs/platform-express';
import { memoryStorage } from 'multer';
import { HiredWorkModule } from './hired-work/hired-work.module';
import { CommentModule } from './comment/comment.module';

@Module({
  imports: [
    AuthModule,
    UserModule,
    ConfigModule.forRoot({ isGlobal: true }),
    WorkTypeModule,
    WorkTypeDetailModule,
    WorkModule,
    // importing MulterModule and use memory storage to use the buffer within the pipe
    MulterModule.register({
      storage: memoryStorage,
    }),
    HiredWorkModule,
    CommentModule,
  ],
  controllers: [AppController],
  providers: [AppService, JwtStrategy],
})
export class AppModule {}
