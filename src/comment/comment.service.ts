import { Injectable } from '@nestjs/common';
import { PrismaClient } from '@prisma/client';
import { AuthService } from 'src/auth/auth.service';
import { errorCode, failCode, successCode } from 'src/config/response';
import { CommentViewModel } from './entities/comment.entity';

@Injectable()
export class CommentService {
  constructor(private authService: AuthService) {}

  private prisma = new PrismaClient();

  private async findCommentById(id: number) {
    return await this.prisma.comment.findFirst({
      where: { id: +id },
    });
  }

  async getComments() {
    try {
      let data = await this.prisma.comment.findMany();
      return successCode('Lấy danh sách bình luận thành công', data);
    } catch ({ response, status }) {
      errorCode(response, status);
    }
  }

  async createComment(
    token: string,
    { work_id, user_id, content, stars }: CommentViewModel,
  ) {
    try {
      // kiểm tra token
      if (this.authService.verifyToken(token)) {
        let newComment = {
          comment_day: new Date(),
          content,
          stars,
          work_id,
          user_id,
        };
        // tạo bình luận mới
        let data = await this.prisma.comment.create({ data: newComment });
        return successCode('Tạo bình luận thành công', data);
      } else {
        failCode('Unauthorized', 400);
      }
    } catch ({ response, status }) {
      errorCode(response, status);
    }
  }

  async updateComment(
    token: string,
    id: number,
    { content, stars }: CommentViewModel,
  ) {
    try {
      // kiểm tra token
      if (this.authService.verifyToken(token)) {
        let comment = await this.findCommentById(id);
        if (comment) {
          let updateComment = { ...comment, content, stars };
          // cập nhật bình luận
          let data = await this.prisma.comment.update({
            data: updateComment,
            where: { id: +id },
          });
          return successCode('Cập nhật bình luận thành công', data);
        } else {
          failCode('Không tìm thấy bình luận', 400);
        }
      } else {
        failCode('Unauthorized', 400);
      }
    } catch ({ response, status }) {
      errorCode(response, status);
    }
  }

  async deleteComment(token: string, id: number) {
    try {
      // kiểm tra token
      if (this.authService.verifyToken(token)) {
        let comment = await this.findCommentById(id);
        if (comment) {
          // xóa bình luận
          let data = await this.prisma.comment.delete({
            where: { id: +id },
          });
          return successCode('Xóa bình luận thành công', data);
        } else {
          failCode('Không tìm thấy bình luận', 400);
        }
      } else {
        failCode('Unauthorized', 400);
      }
    } catch ({ response, status }) {
      errorCode(response, status);
    }
  }

  async getCommentsByWork(id: number) {
    try {
      let work = await this.prisma.work.findFirst({
        where: { id: +id },
        include: { comment: true },
      });
      if (work) {
        return successCode('Lấy bình luận theo công việc thành công', work);
      } else {
        failCode('Không tìm thấy công việc', 400);
      }
    } catch ({ response, status }) {
      errorCode(response, status);
    }
  }
}
