import {
  Body,
  Controller,
  Delete,
  Get,
  Headers,
  HttpCode,
  Param,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { CommentService } from './comment.service';
import {
  ApiBearerAuth,
  ApiBody,
  ApiHeaders,
  ApiParam,
  ApiTags,
} from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { CommentViewModel } from './entities/comment.entity';

@ApiTags('Comment')
@Controller('comment')
export class CommentController {
  constructor(private readonly commentService: CommentService) {}

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(200)
  @Get()
  getComments() {
    return this.commentService.getComments();
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiHeaders([{ name: 'token' }])
  @ApiBody({ type: CommentViewModel })
  @HttpCode(200)
  @Post()
  createComment(
    @Headers('token') token: string,
    @Body() comment: CommentViewModel,
  ) {
    return this.commentService.createComment(token, comment);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiHeaders([{ name: 'token' }])
  @ApiParam({ name: 'id', type: Number })
  @ApiBody({ type: CommentViewModel })
  @HttpCode(200)
  @Put('/:id')
  updateComment(
    @Headers('token') token: string,
    @Param('id') id: number,
    @Body() comment: CommentViewModel,
  ) {
    return this.commentService.updateComment(token, id, comment);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiHeaders([{ name: 'token' }])
  @ApiParam({ name: 'id', type: Number })
  @HttpCode(200)
  @Delete('/:id')
  deleteComment(@Headers('token') token: string, @Param('id') id: number) {
    return this.commentService.deleteComment(token, id);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiParam({ name: 'WorkId', type: Number })
  @HttpCode(200)
  @Get('get-comment-by-work/:WorkId')
  getCommentsByWork(@Param('WorkId') id: number) {
    return this.commentService.getCommentsByWork(id);
  }
}
