import { ApiProperty } from '@nestjs/swagger';

export class CommentViewModel {
  @ApiProperty({ name: 'id', type: Number })
  id: number;

  @ApiProperty({ name: 'work_id', type: Number })
  work_id: number;

  @ApiProperty({ name: 'user_id', type: Number })
  user_id: number;

  @ApiProperty({ name: 'comment_day', type: String })
  comment_day: string;

  @ApiProperty({ name: 'content', type: String })
  content: string;

  @ApiProperty({ name: 'stars', type: Number })
  stars: number;
}
