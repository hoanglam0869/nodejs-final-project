import { HttpException } from '@nestjs/common';

// 200
const successCode = (message: string, data: any) => ({
  message,
  content: data,
});

// 400
const failCode = (response: string, status: number) => {
  throw new HttpException(response, status);
};

// 500
const errorCode = (response: string, status: number) => {
  throw new HttpException(status == 500 ? 'Lỗi BE' : response, status);
};

export { successCode, failCode, errorCode };
