import { UserTypeDto } from 'src/user/dto/user.dto';

export const parseUser = (user: UserTypeDto) => {
  // tách mật khẩu khỏi đối tượng người dùng
  const { password, ...result } = user;
  return {
    ...result,
    skill: JSON.parse(user.skill),
    certification: JSON.parse(user.certification),
  };
};
