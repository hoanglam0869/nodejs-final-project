import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';

export const fileInterceptor = () => {
  return FileInterceptor('file', {
    storage: diskStorage({
      destination: process.cwd() + '/public/img', // nơi lưu hình
      filename: (req, file, callback) => {
        callback(null, new Date().getTime() + file.originalname); // đổi tên hình
      },
    }),
  });
};
