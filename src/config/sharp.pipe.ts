import { Injectable, PipeTransform } from '@nestjs/common';
import * as path from 'path';
import * as sharp from 'sharp';

@Injectable()
class SharpPipe implements PipeTransform<Express.Multer.File, Promise<String>> {
  async transform(image: Express.Multer.File): Promise<string> {
    const originalName = path.parse(image.originalname).name;
    const filename = Date.now() + '-' + originalName + '.webp';

    await sharp(image.buffer)
      .resize(800) // kích thước hình ảnh
      .webp({ effort: 3 }) // chuyển đuôi hình sang .webp (thân thiện với web)
      .toFile(path.join(process.cwd() + '/public/img', filename)); // đường dẫn hình ảnh

    return filename;
  }
}

export { SharpPipe };

// Nén hình ảnh
// https://dev.to/andersonjoseph/nestjs-creating-a-pipe-to-optimize-uploaded-images-5b3h
