import { Body, Controller, HttpCode, Post } from '@nestjs/common';
import { AuthService } from './auth.service';
import { ApiBody, ApiTags } from '@nestjs/swagger';
import { SignInView, UserInfo } from 'src/user/entities/user.entity';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @ApiBody({ type: UserInfo })
  @HttpCode(200)
  @Post('/signup')
  signUp(@Body() user: UserInfo) {
    return this.authService.signUp(user);
  }

  @ApiBody({ type: SignInView })
  @HttpCode(200)
  @Post('/signin')
  signIn(@Body() user: SignInView) {
    return this.authService.signIn(user);
  }
}
