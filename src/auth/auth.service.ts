import { Injectable } from '@nestjs/common';
import { PrismaClient } from '@prisma/client';
import * as bcrypt from 'bcrypt';
import { SignInView, UserInfo } from 'src/user/entities/user.entity';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { errorCode, failCode, successCode } from 'src/config/response';
import { decodeTypeDto } from './dto/auth.dto';
import { parseUser } from 'src/config/user';

@Injectable()
export class AuthService {
  constructor(
    private jwtService: JwtService,
    private configService: ConfigService,
  ) {}

  private prisma = new PrismaClient();

  async signUp({
    name,
    email,
    password,
    phone,
    birth_day,
    gender,
    role,
    skill,
    certification,
  }: UserInfo) {
    try {
      // check email trùng
      let checkUser = await this.prisma.user.findFirst({
        where: { email },
      });
      if (checkUser) {
        // true: báo lỗi email trùng
        failCode('Email đã tồn tại', 400);
      } else {
        // false: tạo mới user
        let newUser = {
          name,
          email,
          password: bcrypt.hashSync(password, 10),
          phone,
          birth_day,
          gender: gender ? 'Nam' : 'Nữ',
          role,
          skill: JSON.stringify(skill),
          certification: JSON.stringify(certification),
          avatar: '',
        };
        let data = await this.prisma.user.create({
          data: newUser,
          include: { hired_work: true },
        });
        return successCode('Đăng ký thành công', parseUser(data));
      }
    } catch ({ response, status }) {
      errorCode(response, status);
    }
  }

  async signIn({ email, password }: SignInView) {
    try {
      // tìm bằng email
      let checkUser = await this.prisma.user.findFirst({
        where: { email },
        include: { hired_work: true },
      });
      if (checkUser) {
        // true: check password
        if (bcrypt.compareSync(password, checkUser.password)) {
          // tạo token
          let token = this.jwtService.sign(
            { data: checkUser },
            { secret: this.configService.get('KEY'), expiresIn: '1d' },
          );
          return successCode('Đăng nhập thành công', token);
        } else {
          // false password: báo mật khẩu không đúng
          failCode('Mật khẩu không đúng!', 400);
        }
      } else {
        // false email: báo mail không tồn tại
        failCode('Email không đúng!', 400);
      }
    } catch ({ response, status }) {
      errorCode(response, status);
    }
  }

  verifyToken(token: string) {
    try {
      this.jwtService.verify(token, {
        secret: this.configService.get('KEY'),
      });
      return true;
    } catch (error) {
      return false;
    }
  }

  decodeToken(token: string) {
    try {
      let res = this.jwtService.decode(token, { json: true }) as decodeTypeDto;
      let { hired_work, ...user } = res.data;
      return user;
    } catch (error) {
      return null;
    }
  }
}
