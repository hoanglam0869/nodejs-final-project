import { UserTypeDto } from 'src/user/dto/user.dto';

export type decodeTypeDto = {
  data: UserTypeDto;
};
