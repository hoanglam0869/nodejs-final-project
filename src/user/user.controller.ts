import {
  Body,
  Controller,
  Delete,
  Get,
  Headers,
  HttpCode,
  Param,
  Post,
  Put,
  Query,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { UserService } from './user.service';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiBearerAuth,
  ApiBody,
  ApiConsumes,
  ApiHeaders,
  ApiParam,
  ApiQuery,
  ApiTags,
} from '@nestjs/swagger';
import { UserUpdate, UserInfo } from './entities/user.entity';
import { FileUploadDto } from './dto/user.dto';
import { SharpPipe } from 'src/config/sharp.pipe';
import { FileInterceptor } from '@nestjs/platform-express';

@ApiTags('User')
@Controller('users')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(200)
  @Get()
  getUsers() {
    return this.userService.getUsers();
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiBody({ type: UserInfo })
  @HttpCode(200)
  @Post()
  createUser(@Body() user: UserInfo) {
    return this.userService.createUser(user);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiQuery({ name: 'id', type: Number })
  @HttpCode(200)
  @Delete()
  deleteUser(@Query('id') id: number) {
    return this.userService.deleteUser(id);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiQuery({ name: 'pageIndex', type: Number })
  @ApiQuery({ name: 'pageSize', type: Number })
  @ApiQuery({ name: 'keyword', type: String, required: false })
  @HttpCode(200)
  @Get('pagination')
  userPagination(
    @Query('pageIndex') pageIndex: number,
    @Query('pageSize') pageSize: number,
    @Query('keyword') keyword: string,
  ) {
    return this.userService.usersPagination(pageIndex, pageSize, keyword);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiParam({ name: 'id', type: Number })
  @HttpCode(200)
  @Get('/:id')
  getUserById(@Param('id') id: number) {
    return this.userService.getUserById(id);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiParam({ name: 'id', type: Number })
  @ApiBody({ type: UserUpdate })
  @HttpCode(200)
  @Put('/:id')
  updateUser(@Param('id') id: number, @Body() user: UserUpdate) {
    return this.userService.updateUser(id, user);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiParam({ name: 'Username', type: String })
  @HttpCode(200)
  @Get('search/:Username')
  searchUsername(@Param('Username') Username: string) {
    return this.userService.searchUsername(Username);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiHeaders([{ name: 'token' }])
  @ApiConsumes('multipart/form-data')
  @ApiBody({ type: FileUploadDto })
  @UseInterceptors(FileInterceptor('file'))
  @HttpCode(200)
  @Post('upload-avatar')
  uploadAvatar(
    @Headers('token') token: string,
    @UploadedFile(SharpPipe) image: string, // nén hình bằng SharpPipe
  ) {
    return this.userService.uploadAvatar(token, image);
  }
}
