import { Injectable } from '@nestjs/common';
import { PrismaClient } from '@prisma/client';
import { UserUpdate, UserInfo } from './entities/user.entity';
import * as bcrypt from 'bcrypt';
import { errorCode, failCode, successCode } from 'src/config/response';
import { AuthService } from 'src/auth/auth.service';
import { parseUser } from 'src/config/user';

@Injectable()
export class UserService {
  constructor(private authService: AuthService) {}

  private prisma = new PrismaClient();

  private async findUserById(id: number) {
    return await this.prisma.user.findFirst({
      where: { id: +id },
      include: { hired_work: true },
    });
  }

  async getUsers() {
    try {
      let users = await this.prisma.user.findMany({
        include: { hired_work: true },
      });
      let data = users.map((user) => parseUser(user));
      return successCode('Lấy danh sách người dùng thành công', data);
    } catch ({ response, status }) {
      errorCode(response, status);
    }
  }

  async createUser({
    name,
    email,
    password,
    phone,
    birth_day,
    gender,
    role,
    skill,
    certification,
  }: UserInfo) {
    try {
      // check email trùng
      let checkUser = await this.prisma.user.findFirst({
        where: { email },
      });
      if (checkUser) {
        // true: báo lỗi email trùng
        failCode('Email đã tồn tại', 400);
      } else {
        // false: tạo mới user
        let newUser = {
          name,
          email,
          password: bcrypt.hashSync(password, 10),
          phone,
          birth_day,
          gender: gender ? 'Nam' : 'Nữ',
          role,
          skill: JSON.stringify(skill),
          certification: JSON.stringify(certification),
          avatar: '',
        };
        let data = await this.prisma.user.create({
          data: newUser,
          include: { hired_work: true },
        });
        return successCode('Thêm người dùng thành công', parseUser(data));
      }
    } catch ({ response, status }) {
      errorCode(response, status);
    }
  }

  async deleteUser(id: number) {
    try {
      let checkUser = await this.findUserById(id);
      if (checkUser) {
        let data = await this.prisma.user.delete({
          where: { id: +id },
          include: { hired_work: true },
        });
        return successCode('Xóa người dùng thành công', parseUser(data));
      } else {
        failCode('Không tìm thấy người dùng', 400);
      }
    } catch ({ response, status }) {
      errorCode(response, status);
    }
  }

  async usersPagination(pageIndex: number, pageSize: number, keyword: string) {
    try {
      let skip = (+pageIndex - 1) * +pageSize;

      let checkUsers = await this.prisma.user.findMany({
        skip: +skip, // số dòng được bỏ qua
        take: +pageSize, // số dòng sẽ lấy trong 1 trang
        where: {
          name: { contains: keyword },
        },
        include: { hired_work: true },
      });
      if (checkUsers.length > 0) {
        let data = checkUsers.map((user) => parseUser(user));
        return successCode(
          'Lấy danh sách người dùng phân trang thành công',
          data,
        );
      } else {
        failCode('Không tìm thấy danh sách người dùng', 400);
      }
    } catch ({ response, status }) {
      errorCode(response, status);
    }
  }

  async getUserById(id: number) {
    try {
      let checkUser = await this.findUserById(id);
      if (checkUser) {
        return successCode(
          'Lấy thông tin người dùng thành công',
          parseUser(checkUser),
        );
      } else {
        failCode('Không tìm thấy người dùng', 400);
      }
    } catch ({ response, status }) {
      errorCode(response, status);
    }
  }

  async updateUser(
    id: number,
    {
      name,
      email,
      phone,
      birth_day,
      gender,
      role,
      skill,
      certification,
    }: UserUpdate,
  ) {
    try {
      let checkUser = await this.findUserById(id);
      if (checkUser) {
        let updateUser = {
          name,
          email,
          phone,
          birth_day,
          gender: gender ? 'Nam' : 'Nữ',
          role,
          skill: JSON.stringify(skill),
          certification: JSON.stringify(certification),
        };
        let data = await this.prisma.user.update({
          data: updateUser,
          where: { id: +id },
          include: { hired_work: true },
        });
        return successCode('Cập nhật người dùng thành công', parseUser(data));
      } else {
        failCode('Không tìm thấy người dùng', 400);
      }
    } catch ({ response, status }) {
      errorCode(response, status);
    }
  }

  async searchUsername(name: string) {
    try {
      let checkUsers = await this.prisma.user.findMany({
        where: {
          name: { contains: name },
        },
        include: { hired_work: true },
      });
      if (checkUsers.length > 0) {
        let data = checkUsers.map((user) => parseUser(user));
        return successCode(
          'Tìm danh sách người dùng theo tên thành công',
          data,
        );
      } else {
        failCode('Không tìm thấy danh sách người dùng', 400);
      }
    } catch ({ response, status }) {
      errorCode(response, status);
    }
  }

  async uploadAvatar(token: string, image: string) {
    try {
      // giải mã token
      let user = this.authService.decodeToken(token);
      if (user) {
        // cập nhật dữ liệu mới
        user.avatar = image;
        // update dữ liệu vào database
        let data = await this.prisma.user.update({
          data: user,
          where: { id: +user.id },
          include: { hired_work: true },
        });
        return successCode('Tải hình đại diện lên thành công', parseUser(data));
      } else {
        failCode('Unauthorized', 400);
      }
    } catch ({ response, status }) {
      errorCode(response, status);
    }
  }
}
