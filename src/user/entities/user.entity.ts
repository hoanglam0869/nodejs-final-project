import { ApiProperty } from '@nestjs/swagger';
import { IsEmail } from 'class-validator';

export class UserInfo {
  @ApiProperty({ description: 'id', type: Number })
  id: number;

  @ApiProperty({ description: 'name', type: String })
  name: string;

  @IsEmail()
  @ApiProperty({ description: 'email', type: String })
  email: string;

  @ApiProperty({ description: 'password', type: String })
  password: string;

  @ApiProperty({ description: 'phone', type: String })
  phone: string;

  @ApiProperty({ description: 'birth_day', type: String })
  birth_day: string;

  @ApiProperty({ description: 'gender', type: Boolean })
  gender: boolean;

  @ApiProperty({ description: 'role', type: String })
  role: string;

  @ApiProperty({ description: 'skill', type: [String] })
  skill: string[];

  @ApiProperty({ description: 'certification', type: [String] })
  certification: string[];
}

export class SignInView {
  @ApiProperty({ description: 'email', type: String })
  email: string;

  @ApiProperty({ description: 'password', type: String })
  password: string;
}

export class UserUpdate {
  @ApiProperty({ description: 'id', type: Number })
  id: number;

  @ApiProperty({ description: 'name', type: String })
  name: string;

  @ApiProperty({ description: 'email', type: String })
  email: string;

  @ApiProperty({ description: 'phone', type: String })
  phone: string;

  @ApiProperty({ description: 'birth_day', type: String })
  birth_day: string;

  @ApiProperty({ description: 'gender', type: Boolean })
  gender: boolean;

  @ApiProperty({ description: 'role', type: String })
  role: string;

  @ApiProperty({ description: 'skill', type: [String] })
  skill: string[];

  @ApiProperty({ description: 'certification', type: [String] })
  certification: string[];
}
