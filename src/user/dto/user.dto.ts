import { ApiProperty } from '@nestjs/swagger';

export type UserTypeDto = {
  id: number;
  name: string;
  email: string;
  password: string;
  phone: string;
  birth_day: string;
  gender: string;
  role: string;
  skill: string;
  certification: string;
  avatar: string;
  hired_work?: any[];
};

export class FileUploadDto {
  @ApiProperty({ type: 'string', format: 'binary' })
  file: any;
}
