import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy, 'jwt') {
  // kiểm tra token
  constructor(config: ConfigService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: config.get('KEY'),
    });
  }

  // trả về dữ liệu khi một API gọi thành công có chứa token
  async validate(tokenDecode: any) {
    // check quyền
    return tokenDecode;
  }
}

// Reference: https://progressivecoder.com/how-to-implement-nestjs-jwt-authentication-using-jwt-strategy/#google_vignette
