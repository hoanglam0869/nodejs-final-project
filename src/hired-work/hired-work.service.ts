import { Injectable } from '@nestjs/common';
import { PrismaClient } from '@prisma/client';
import { HiredWorkViewModel } from './entities/hired-work.entity';
import { errorCode, failCode, successCode } from 'src/config/response';
import { AuthService } from 'src/auth/auth.service';
import { parseUser } from 'src/config/user';

@Injectable()
export class HiredWorkService {
  constructor(private authService: AuthService) {}

  private prisma = new PrismaClient();

  private async findHiredWorkById(id: number) {
    return await this.prisma.hired_work.findFirst({
      where: { id: +id },
    });
  }

  async getHiredWorks() {
    try {
      let data = await this.prisma.hired_work.findMany();
      return successCode('Lấy danh sách công việc đã thuê thành công', data);
    } catch ({ response, status }) {
      errorCode(response, status);
    }
  }

  async createHiredWork(
    token: string,
    { work_id, user_id }: HiredWorkViewModel,
  ) {
    try {
      // kiểm tra token
      if (this.authService.verifyToken(token)) {
        let hiredWork = {
          work_id,
          user_id,
          hire_day: new Date(),
          is_finished: false,
        };
        // tạo công việc đã thuê mới
        let data = await this.prisma.hired_work.create({ data: hiredWork });
        return successCode('Tạo công việc đã thuê thành công', data);
      } else {
        failCode('Unauthorized', 400);
      }
    } catch ({ response, status }) {
      errorCode(response, status);
    }
  }

  async hiredWorksPagination(
    pageIndex: number,
    pageSize: number,
    keyword: string,
  ) {
    try {
      let skip = (+pageIndex - 1) * +pageSize;
      // phân trang
      let hiredWorks = await this.prisma.hired_work.findMany({
        skip: +skip, // số dòng được bỏ qua
        take: +pageSize, // số dòng sẽ lấy trong 1 trang
        include: { work: true },
        where: {
          work: {
            name: { contains: keyword },
          },
        },
      });
      if (hiredWorks.length > 0) {
        return successCode(
          'Lấy danh sách công việc đã thuê phân trang thành công',
          hiredWorks,
        );
      } else {
        failCode('Không tìm thấy danh sách công việc đã thuê', 400);
      }
    } catch ({ response, status }) {
      errorCode(response, status);
    }
  }

  async getHiredWorkById(id: number) {
    try {
      let hiredWork = await this.findHiredWorkById(id);
      if (hiredWork) {
        return successCode(
          'Lấy thông tin công việc đã thuê thành công',
          hiredWork,
        );
      } else {
        failCode('Không tìm thấy công việc đã thuê', 400);
      }
    } catch ({ response, status }) {
      errorCode(response, status);
    }
  }

  async updateHiredWork(
    token: string,
    id: number,
    { hire_day, work_id, user_id }: HiredWorkViewModel,
  ) {
    try {
      // kiểm tra token
      if (this.authService.verifyToken(token)) {
        let hiredWork = await this.findHiredWorkById(id);
        if (hiredWork) {
          let updateHiredWork = {
            ...hiredWork,
            hire_day,
            work_id,
            user_id,
          };
          // cập nhật công việc đã thuê
          let data = await this.prisma.hired_work.update({
            data: updateHiredWork,
            where: { id: +id },
          });
          return successCode('Cập nhật công việc đã thuê thành công', data);
        } else {
          failCode('Không tìm thấy công việc đã thuê', 400);
        }
      } else {
        failCode('Unauthorized', 400);
      }
    } catch ({ response, status }) {
      errorCode(response, status);
    }
  }

  async deleteHiredWork(token: string, id: number) {
    try {
      // kiểm tra token
      if (this.authService.verifyToken(token)) {
        let hiredWork = await this.findHiredWorkById(id);
        if (hiredWork) {
          // xóa công việc đã thuê
          let data = await this.prisma.hired_work.delete({
            where: { id: +id },
          });
          return successCode('Xóa công việc đã thuê thành công', data);
        } else {
          failCode('Không tìm thấy công việc đã thuê', 400);
        }
      } else {
        failCode('Unauthorized', 400);
      }
    } catch ({ response, status }) {
      errorCode(response, status);
    }
  }

  async getHiredWorksByUser(token: string) {
    try {
      // giải mã token
      let user = this.authService.decodeToken(token);
      if (user) {
        // tìm danh sách công việc người dùng thuê
        let hiredWorks = await this.prisma.hired_work.findMany({
          where: { user_id: user.id },
        });
        if (hiredWorks.length > 0) {
          let data = { ...parseUser(user), hired_work: hiredWorks };
          return successCode(
            'Lấy danh sách công việc đã thuê thành công',
            data,
          );
        } else {
          failCode('Không tìm thấy danh sách công việc đã thuê', 400);
        }
      } else {
        failCode('Unauthorized', 400);
      }
    } catch ({ response, status }) {
      errorCode(response, status);
    }
  }

  async finishWork(id: number) {
    try {
      let hiredWork = await this.findHiredWorkById(id);
      if (hiredWork) {
        // Hoàn thành công việc đã thuê
        hiredWork.is_finished = true;
        // Cập nhật công việc đã thuê
        let data = await this.prisma.hired_work.update({
          where: { id: +id },
          data: hiredWork,
        });
        return successCode('Hoàn thành công việc đã thuê', data);
      } else {
        failCode('Không tìm thấy công việc đã thuê', 400);
      }
    } catch ({ response, status }) {
      errorCode(response, status);
    }
  }
}
