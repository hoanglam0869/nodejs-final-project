import { ApiProperty } from '@nestjs/swagger';

export class HiredWorkViewModel {
  @ApiProperty({ name: 'id', type: Number })
  id: number;

  @ApiProperty({ name: 'work_id', type: Number })
  work_id: number;

  @ApiProperty({ name: 'user_id', type: Number })
  user_id: number;

  @ApiProperty({ name: 'hire_day', type: String })
  hire_day: string;

  @ApiProperty({ name: 'is_finished', type: Boolean })
  is_finished: boolean;
}
