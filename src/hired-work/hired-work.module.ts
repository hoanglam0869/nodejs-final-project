import { Module } from '@nestjs/common';
import { HiredWorkService } from './hired-work.service';
import { HiredWorkController } from './hired-work.controller';
import { AuthModule } from 'src/auth/auth.module';

@Module({
  imports: [AuthModule],
  controllers: [HiredWorkController],
  providers: [HiredWorkService],
})
export class HiredWorkModule {}
