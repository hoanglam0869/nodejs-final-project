export type HiredWorkDto = {
  id: number;
  hire_day: string;
  is_finished: boolean;
  work_id: number;
  user_id: number;
};
