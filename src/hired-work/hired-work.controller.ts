import {
  Body,
  Controller,
  Delete,
  Get,
  Headers,
  HttpCode,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import { HiredWorkService } from './hired-work.service';
import {
  ApiBearerAuth,
  ApiBody,
  ApiHeaders,
  ApiParam,
  ApiQuery,
  ApiTags,
} from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { HiredWorkViewModel } from './entities/hired-work.entity';

@ApiTags('HiredWork')
@Controller('hired-work')
export class HiredWorkController {
  constructor(private readonly HiredWorkService: HiredWorkService) {}

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(200)
  @Get()
  getHiredWorks() {
    return this.HiredWorkService.getHiredWorks();
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiHeaders([{ name: 'token' }])
  @ApiBody({ type: HiredWorkViewModel })
  @HttpCode(200)
  @Post()
  createHiredWork(
    @Headers('token') token: string,
    @Body() hiredWork: HiredWorkViewModel,
  ) {
    return this.HiredWorkService.createHiredWork(token, hiredWork);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiQuery({ name: 'pageIndex', type: Number })
  @ApiQuery({ name: 'pageSize', type: Number })
  @ApiQuery({ name: 'keyword', type: String, required: false })
  @HttpCode(200)
  @Get('pagination')
  hiredWorksPagination(
    @Query('pageIndex') pageIndex: number,
    @Query('pageSize') pageSize: number,
    @Query('keyword') keyword: string,
  ) {
    return this.HiredWorkService.hiredWorksPagination(
      pageIndex,
      pageSize,
      keyword,
    );
  }

  // Lưu ý: getHiredWorksByUser phải đặt trước getHiredWorkById vì nếu đặt sau thì trình biên dịch sẽ hiểu nhầm endpoint 'get-hired-work-by-user' là '/:id'
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiHeaders([{ name: 'token' }])
  @HttpCode(200)
  @Get('get-hired-work-by-user')
  getHiredWorksByUser(@Headers('token') token: string) {
    return this.HiredWorkService.getHiredWorksByUser(token);
  }

  // Đặt getWorkHireById sau getHiredWorks
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiParam({ name: 'id', type: Number })
  @HttpCode(200)
  @Get('/:id')
  getHiredWorkById(@Param('id') id: number) {
    return this.HiredWorkService.getHiredWorkById(id);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiHeaders([{ name: 'token' }])
  @ApiParam({ name: 'id', type: Number })
  @ApiBody({ type: HiredWorkViewModel })
  @HttpCode(200)
  @Put('/:id')
  updateHiredWork(
    @Headers('token') token: string,
    @Param('id') id: number,
    @Body() hiredWork: HiredWorkViewModel,
  ) {
    return this.HiredWorkService.updateHiredWork(token, id, hiredWork);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiHeaders([{ name: 'token' }])
  @ApiParam({ name: 'id', type: Number })
  @HttpCode(200)
  @Delete('/:id')
  deleteHiredWork(@Headers('token') token: string, @Param('id') id: number) {
    return this.HiredWorkService.deleteHiredWork(token, id);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiParam({ name: 'WorkId', type: Number })
  @HttpCode(200)
  @Put('finish-work/:WorkId')
  finishWork(@Param('WorkId') id: number) {
    return this.HiredWorkService.finishWork(id);
  }
}
