import {
  Body,
  Controller,
  Delete,
  Get,
  Headers,
  HttpCode,
  Param,
  Post,
  Put,
  Query,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { WorkTypeDetailService } from './work-type-detail.service';
import {
  ApiBearerAuth,
  ApiBody,
  ApiConsumes,
  ApiHeaders,
  ApiParam,
  ApiQuery,
  ApiTags,
} from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { WorkTypeDetailViewModel } from './entities/work-type-detail.entity';
import { FileUploadDto } from 'src/user/dto/user.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { SharpPipe } from 'src/config/sharp.pipe';

@ApiTags('WorkTypeDetail')
@Controller('work-type-detail')
export class WorkTypeDetailController {
  constructor(private readonly workTypeDetailService: WorkTypeDetailService) {}

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(200)
  @Get()
  getWorkTypes() {
    return this.workTypeDetailService.getWorkTypeDetails();
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiHeaders([{ name: 'token' }])
  @ApiBody({ type: WorkTypeDetailViewModel })
  @HttpCode(200)
  @Post()
  createWorkType(
    @Headers('token') token: string,
    @Body() workTypeDetail: WorkTypeDetailViewModel,
  ) {
    return this.workTypeDetailService.createWorkTypeDetail(
      token,
      workTypeDetail,
    );
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiQuery({ name: 'pageIndex', type: Number })
  @ApiQuery({ name: 'pageSize', type: Number })
  @ApiQuery({ name: 'keyword', type: String, required: false })
  @HttpCode(200)
  @Get('pagination')
  workTypeDetailsPagination(
    @Query('pageIndex') pageIndex: number,
    @Query('pageSize') pageSize: number,
    @Query('keyword') keyword: string,
  ) {
    return this.workTypeDetailService.workTypeDetailsPagination(
      pageIndex,
      pageSize,
      keyword,
    );
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiParam({ name: 'id', type: Number })
  @HttpCode(200)
  @Get('/:id')
  getUserById(@Param('id') id: number) {
    return this.workTypeDetailService.getWorkTypeDetailById(id);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiHeaders([{ name: 'token' }])
  @ApiParam({ name: 'id', type: Number })
  @ApiBody({ type: WorkTypeDetailViewModel })
  @HttpCode(200)
  @Put('/:id')
  updateUser(
    @Headers('token') token: string,
    @Param('id') id: number,
    @Body() workTypeDetail: WorkTypeDetailViewModel,
  ) {
    return this.workTypeDetailService.updateWorkTypeDetail(
      token,
      id,
      workTypeDetail,
    );
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiHeaders([{ name: 'token' }])
  @ApiParam({ name: 'id', type: Number })
  @HttpCode(200)
  @Delete('/:id')
  deleteUser(@Headers('token') token: string, @Param('id') id: number) {
    return this.workTypeDetailService.deleteWorkTypeDetail(token, id);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiHeaders([{ name: 'token' }])
  @ApiParam({ name: 'id', type: Number })
  @ApiConsumes('multipart/form-data')
  @ApiBody({ type: FileUploadDto })
  @UseInterceptors(FileInterceptor('file'))
  @HttpCode(200)
  @Post('upload-image/:id')
  uploadWorkTypeDetail(
    @Headers('token') token: string,
    @Param('id') id: number,
    @UploadedFile(SharpPipe) image: string, // nén hình bằng SharpPipe
  ) {
    return this.workTypeDetailService.uploadWorkTypeDetail(token, id, image);
  }
}
