import { Module } from '@nestjs/common';
import { WorkTypeDetailService } from './work-type-detail.service';
import { WorkTypeDetailController } from './work-type-detail.controller';
import { AuthModule } from 'src/auth/auth.module';

@Module({
  imports: [AuthModule],
  controllers: [WorkTypeDetailController],
  providers: [WorkTypeDetailService],
})
export class WorkTypeDetailModule {}
