import { ApiProperty } from '@nestjs/swagger';

export class WorkTypeDetailViewModel {
  @ApiProperty({ name: 'id', type: Number })
  id: number;

  @ApiProperty({ name: 'name', type: String })
  name: string;

  @ApiProperty({ name: 'work_type_id', type: Number })
  work_type_id: number;
}
