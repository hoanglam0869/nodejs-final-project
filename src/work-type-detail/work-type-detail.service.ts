import { Injectable } from '@nestjs/common';
import { PrismaClient } from '@prisma/client';
import { WorkTypeDetailViewModel } from './entities/work-type-detail.entity';
import { errorCode, failCode, successCode } from 'src/config/response';
import { AuthService } from 'src/auth/auth.service';

@Injectable()
export class WorkTypeDetailService {
  constructor(private authService: AuthService) {}

  private prisma = new PrismaClient();

  private async findWorkTypeDetailById(id: number) {
    return await this.prisma.work_type_detail.findFirst({
      where: { id: +id },
    });
  }

  async getWorkTypeDetails() {
    try {
      let data = await this.prisma.work_type_detail.findMany();
      return successCode(
        'Lấy danh sách chi tiết loại công việc thành công',
        data,
      );
    } catch ({ response, status }) {
      errorCode(response, status);
    }
  }

  async createWorkTypeDetail(
    token: string,
    { name, work_type_id }: WorkTypeDetailViewModel,
  ) {
    try {
      // kiểm tra token
      if (this.authService.verifyToken(token)) {
        // tạo chi tiết loại công việc mới
        let workTypeDetail = {
          name,
          work_type_id: +work_type_id,
          image: '',
        };
        let data = await this.prisma.work_type_detail.create({
          data: workTypeDetail,
        });
        return successCode('Tạo chi tiết loại công việc thành công', data);
      } else {
        failCode('Unauthorized', 400);
      }
    } catch ({ response, status }) {
      errorCode(response, status);
    }
  }

  async workTypeDetailsPagination(
    pageIndex: number,
    pageSize: number,
    keyword: string,
  ) {
    try {
      let skip = (+pageIndex - 1) * +pageSize;
      // phân trang
      let workTypeDetails = await this.prisma.work_type_detail.findMany({
        skip: +skip, // số dòng được bỏ qua
        take: +pageSize, // số dòng sẽ lấy trong 1 trang
        where: {
          name: { contains: keyword },
        },
      });
      if (workTypeDetails.length > 0) {
        return successCode(
          'Lấy danh sách chi tiết loại công việc phân trang thành công',
          workTypeDetails,
        );
      } else {
        failCode('Không tìm thấy danh sách chi tiết loại công việc', 400);
      }
    } catch ({ response, status }) {
      errorCode(response, status);
    }
  }

  async getWorkTypeDetailById(id: number) {
    try {
      let workType = await this.findWorkTypeDetailById(id);
      if (workType) {
        return successCode(
          'Lấy thông tin chi tiết loại công việc thành công',
          workType,
        );
      } else {
        failCode('Không tìm thấy chi tiết loại công việc', 400);
      }
    } catch ({ response, status }) {
      errorCode(response, status);
    }
  }

  async updateWorkTypeDetail(
    token: string,
    id: number,
    { name, work_type_id }: WorkTypeDetailViewModel,
  ) {
    try {
      // kiểm tra token
      if (this.authService.verifyToken(token)) {
        let workTypeDetail = await this.findWorkTypeDetailById(id);
        if (workTypeDetail) {
          workTypeDetail = {
            ...workTypeDetail,
            name,
            work_type_id,
          };
          // cập nhật chi tiết loại công việc
          let data = await this.prisma.work_type_detail.update({
            data: workTypeDetail,
            where: { id: +id },
          });
          return successCode(
            'Cập nhật chi tiết loại công việc thành công',
            data,
          );
        } else {
          failCode('Không tìm thấy chi tiết loại công việc', 400);
        }
      } else {
        failCode('Unauthorized', 400);
      }
    } catch ({ response, status }) {
      errorCode(response, status);
    }
  }

  async deleteWorkTypeDetail(token: string, id: number) {
    try {
      // kiểm tra token
      if (this.authService.verifyToken(token)) {
        let workTypeDetail = await this.findWorkTypeDetailById(id);
        if (workTypeDetail) {
          // xóa chi tiết loại công việc
          let data = await this.prisma.work_type_detail.delete({
            where: { id: +id },
          });
          return successCode('Xóa chi tiết loại công việc thành công', data);
        } else {
          failCode('Không tìm thấy chi tiết loại công việc', 400);
        }
      } else {
        failCode('Unauthorized', 400);
      }
    } catch ({ response, status }) {
      errorCode(response, status);
    }
  }

  async uploadWorkTypeDetail(token: string, id: number, image: string) {
    try {
      // kiểm tra token
      if (this.authService.verifyToken(token)) {
        // tìm chi tiết loại công việc muốn update hình
        let workTypeDetail = await this.findWorkTypeDetailById(id);
        if (workTypeDetail) {
          // cập nhật dữ liệu mới
          workTypeDetail.image = image;
          // update dữ liệu vào database
          let data = await this.prisma.work_type_detail.update({
            data: workTypeDetail,
            where: { id: +id },
          });
          return successCode(
            'Tải hình chi tiết loại công việc thành công',
            data,
          );
        } else {
          failCode('Không tìm thấy chi tiết loại công việc', 400);
        }
      } else {
        failCode('Unauthorized', 400);
      }
    } catch ({ response, status }) {
      errorCode(response, status);
    }
  }
}
