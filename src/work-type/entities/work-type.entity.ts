import { ApiProperty } from '@nestjs/swagger';

export class WorkTypeViewModel {
  @ApiProperty({ name: 'id', type: Number })
  id: number;

  @ApiProperty({ name: 'name', type: String })
  name: string;
}
