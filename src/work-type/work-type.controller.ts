import {
  Body,
  Controller,
  Delete,
  Get,
  Headers,
  HttpCode,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import { WorkTypeService } from './work-type.service';
import {
  ApiBearerAuth,
  ApiBody,
  ApiHeaders,
  ApiParam,
  ApiQuery,
  ApiTags,
} from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { WorkTypeViewModel } from './entities/work-type.entity';

@ApiTags('WorkType')
@Controller('work-type')
export class WorkTypeController {
  constructor(private readonly workTypeService: WorkTypeService) {}

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(200)
  @Get()
  getWorkTypes() {
    return this.workTypeService.getWorkTypes();
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiHeaders([{ name: 'token' }])
  @ApiBody({ type: WorkTypeViewModel })
  @HttpCode(200)
  @Post()
  createWorkType(
    @Headers('token') token: string,
    @Body() workType: WorkTypeViewModel,
  ) {
    return this.workTypeService.createWorkType(token, workType);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiQuery({ name: 'pageIndex', type: Number })
  @ApiQuery({ name: 'pageSize', type: Number })
  @ApiQuery({ name: 'keyword', type: String, required: false })
  @HttpCode(200)
  @Get('pagination')
  workTypesPagination(
    @Query('pageIndex') pageIndex: number,
    @Query('pageSize') pageSize: number,
    @Query('keyword') keyword: string,
  ) {
    return this.workTypeService.workTypesPagination(
      pageIndex,
      pageSize,
      keyword,
    );
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiParam({ name: 'id', type: Number })
  @HttpCode(200)
  @Get('/:id')
  getWorkTypeById(@Param('id') id: number) {
    return this.workTypeService.getWorkTypeById(id);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiHeaders([{ name: 'token' }])
  @ApiParam({ name: 'id', type: Number })
  @ApiBody({ type: WorkTypeViewModel })
  @HttpCode(200)
  @Put('/:id')
  updateWorkType(
    @Headers('token') token: string,
    @Param('id') id: number,
    @Body() workType: WorkTypeViewModel,
  ) {
    return this.workTypeService.updateWorkType(token, id, workType);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiHeaders([{ name: 'token' }])
  @ApiParam({ name: 'id', type: Number })
  @HttpCode(200)
  @Delete('/:id')
  deleteWorkType(@Headers('token') token: string, @Param('id') id: number) {
    return this.workTypeService.deleteWorkType(token, id);
  }
}
