import { Injectable } from '@nestjs/common';
import { PrismaClient } from '@prisma/client';
import { WorkTypeViewModel } from './entities/work-type.entity';
import { errorCode, failCode, successCode } from 'src/config/response';
import { AuthService } from 'src/auth/auth.service';

@Injectable()
export class WorkTypeService {
  constructor(private authService: AuthService) {}

  private prisma = new PrismaClient();

  private async findWorkTypeById(id: number) {
    return await this.prisma.work_type.findFirst({
      where: { id: +id },
    });
  }

  async getWorkTypes() {
    try {
      let data = await this.prisma.work_type.findMany();
      return successCode('Lấy danh sách loại công việc thành công', data);
    } catch ({ response, status }) {
      errorCode(response, status);
    }
  }

  async createWorkType(token: string, { name }: WorkTypeViewModel) {
    try {
      // kiểm tra token
      if (this.authService.verifyToken(token)) {
        let workType = { name };
        // tạo loại công việc mới
        let data = await this.prisma.work_type.create({ data: workType });
        return successCode('Tạo loại công việc thành công', data);
      } else {
        failCode('Unauthorized', 400);
      }
    } catch ({ response, status }) {
      errorCode(response, status);
    }
  }

  async workTypesPagination(
    pageIndex: number,
    pageSize: number,
    keyword: string,
  ) {
    try {
      let skip = (+pageIndex - 1) * +pageSize;

      let workTypes = await this.prisma.work_type.findMany({
        skip: +skip, // số dòng được bỏ qua
        take: +pageSize, // số dòng sẽ lấy trong 1 trang
        where: {
          name: { contains: keyword },
        },
      });
      if (workTypes.length > 0) {
        return successCode(
          'Lấy danh sách loại công việc phân trang thành công',
          workTypes,
        );
      } else {
        failCode('Không tìm thấy danh sách loại công việc', 400);
      }
    } catch ({ response, status }) {
      errorCode(response, status);
    }
  }

  async getWorkTypeById(id: number) {
    try {
      let workType = await this.findWorkTypeById(id);
      if (workType) {
        return successCode('Lấy thông tin loại công việc thành công', workType);
      } else {
        failCode('Không tìm thấy loại công việc', 400);
      }
    } catch ({ response, status }) {
      errorCode(response, status);
    }
  }

  async updateWorkType(token: string, id: number, { name }: WorkTypeViewModel) {
    try {
      // kiểm tra token
      if (this.authService.verifyToken(token)) {
        let workType = await this.findWorkTypeById(id);
        if (workType) {
          workType.name = name;
          // cập nhật loại công việc
          let data = await this.prisma.work_type.update({
            data: workType,
            where: { id: +id },
          });
          return successCode('Cập nhật loại công việc thành công', data);
        } else {
          failCode('Không tìm thấy loại công việc', 400);
        }
      } else {
        failCode('Unauthorized', 400);
      }
    } catch ({ response, status }) {
      errorCode(response, status);
    }
  }

  async deleteWorkType(token: string, id: number) {
    try {
      // kiểm tra token
      if (this.authService.verifyToken(token)) {
        let workType = await this.findWorkTypeById(id);
        if (workType) {
          // xóa loại công việc
          let data = await this.prisma.work_type.delete({
            where: { id: +id },
          });
          return successCode('Xóa loại công việc thành công', data);
        } else {
          failCode('Không tìm thấy loại công việc', 400);
        }
      } else {
        failCode('Unauthorized', 400);
      }
    } catch ({ response, status }) {
      errorCode(response, status);
    }
  }
}
