import { Injectable } from '@nestjs/common';
import { PrismaClient } from '@prisma/client';
import { WorkViewModel } from './entities/work.entity';
import { errorCode, failCode, successCode } from 'src/config/response';
import { AuthService } from 'src/auth/auth.service';

@Injectable()
export class WorkService {
  constructor(private authService: AuthService) {}

  private prisma = new PrismaClient();

  private async findWorkById(id: number) {
    return await this.prisma.work.findFirst({
      where: { id: +id },
    });
  }

  async getWorks() {
    try {
      let data = await this.prisma.work.findMany();
      return successCode('Lấy danh sách công việc thành công', data);
    } catch ({ response, status }) {
      errorCode(response, status);
    }
  }

  async createWork(
    token: string,
    {
      name,
      rate,
      price,
      user_id,
      description,
      work_type_detail_id,
      short_desc,
      stars,
    }: WorkViewModel,
  ) {
    try {
      // kiểm tra token
      if (this.authService.verifyToken(token)) {
        let work = {
          name,
          rate: +rate,
          price: +price,
          image: '',
          description,
          short_desc,
          stars: +stars,
          work_type_detail_id: +work_type_detail_id,
          user_id: +user_id,
        };
        // tạo công việc mới
        let data = await this.prisma.work.create({ data: work });
        return successCode('Tạo công việc thành công', data);
      } else {
        failCode('Unauthorized', 400);
      }
    } catch ({ response, status }) {
      errorCode(response, status);
    }
  }

  async worksPagination(pageIndex: number, pageSize: number, keyword: string) {
    try {
      let skip = (+pageIndex - 1) * +pageSize;
      // phân trang
      let works = await this.prisma.work.findMany({
        skip: +skip, // số dòng được bỏ qua
        take: +pageSize, // số dòng sẽ lấy trong 1 trang
        where: {
          name: { contains: keyword },
        },
      });
      if (works.length > 0) {
        return successCode(
          'Lấy danh sách công việc phân trang thành công',
          works,
        );
      } else {
        failCode('Không tìm thấy danh sách công việc', 400);
      }
    } catch ({ response, status }) {
      errorCode(response, status);
    }
  }

  async getWorkById(id: number) {
    try {
      let work = await this.findWorkById(id);
      if (work) {
        return successCode('Lấy thông tin công việc thành công', work);
      } else {
        failCode('Không tìm thấy công việc', 400);
      }
    } catch ({ response, status }) {
      errorCode(response, status);
    }
  }

  async updateWork(
    token: string,
    id: number,
    {
      name,
      rate,
      price,
      user_id,
      description,
      work_type_detail_id,
      short_desc,
      stars,
    }: WorkViewModel,
  ) {
    try {
      // kiểm tra token
      if (this.authService.verifyToken(token)) {
        let work = await this.findWorkById(id);
        if (work) {
          let updateWork = {
            ...work,
            name,
            rate: +rate,
            price: +price,
            description,
            short_desc,
            stars: +stars,
            work_type_detail_id: +work_type_detail_id,
            user_id: +user_id,
          };
          // cập nhật chi tiết loại công việc
          let data = await this.prisma.work.update({
            data: updateWork,
            where: { id: +id },
          });
          return successCode('Cập nhật công việc thành công', data);
        } else {
          failCode('Không tìm thấy công việc', 400);
        }
      } else {
        failCode('Unauthorized', 400);
      }
    } catch ({ response, status }) {
      errorCode(response, status);
    }
  }

  async deleteWork(token: string, id: number) {
    try {
      // kiểm tra token
      if (this.authService.verifyToken(token)) {
        let work = await this.findWorkById(id);
        if (work) {
          // xóa công việc
          let data = await this.prisma.work.delete({
            where: { id: +id },
          });
          return successCode('Xóa công việc thành công', data);
        } else {
          failCode('Không tìm thấy công việc', 400);
        }
      } else {
        failCode('Unauthorized', 400);
      }
    } catch ({ response, status }) {
      errorCode(response, status);
    }
  }

  async uploadWorkImage(token: string, id: number, image: string) {
    try {
      // kiểm tra token
      if (this.authService.verifyToken(token)) {
        // tìm công việc muốn update hình
        let work = await this.findWorkById(id);
        if (work) {
          // cập nhật hình ảnh mới
          work.image = image;
          // update dữ liệu vào database
          let data = await this.prisma.work.update({
            data: work,
            where: { id: +id },
          });
          return successCode('Tải hình công việc thành công', data);
        } else {
          failCode('Không tìm thấy công việc', 400);
        }
      } else {
        failCode('Unauthorized', 400);
      }
    } catch ({ response, status }) {
      errorCode(response, status);
    }
  }

  async getWorkTypesMenu() {
    try {
      let workTypes = await this.prisma.work_type.findMany({
        include: { work_type_detail: true },
      });
      if (workTypes.length > 0) {
        return successCode('Lấy menu loại công việc thành công', workTypes);
      } else {
        failCode('Không tìm thấy menu loại công việc', 400);
      }
    } catch ({ response, status }) {
      errorCode(response, status);
    }
  }

  async getWorkTypeDetails(id: number) {
    try {
      let workType = await this.prisma.work_type.findFirst({
        where: { id: +id },
        include: { work_type_detail: true },
      });
      if (workType) {
        return successCode(
          'Lấy danh sách chi tiết loại công việc thành công',
          workType,
        );
      } else {
        failCode('Không tìm thấy danh sách chi tiết loại công việc', 400);
      }
    } catch ({ response, status }) {
      errorCode(response, status);
    }
  }

  async getWorksByWorkTypeDetailId(id: number) {
    try {
      let works = await this.prisma.work.findMany({
        where: { work_type_detail_id: +id },
        include: {
          work_type_detail: {
            include: { work_type: true },
          },
          user: true,
        },
      });
      if (works.length > 0) {
        let data = works.map((work) => {
          let { work_type_detail, user, ...result } = work;
          let { work_type, name } = work_type_detail;
          return {
            id: work.id,
            work: result,
            work_type_name: work_type.name,
            work_type_detail_name: name,
            user_name: user.name,
            avatar: user.avatar,
          };
        });
        return successCode('Lấy danh sách công việc thành công', data);
      } else {
        failCode('Không tìm thấy danh sách công việc', 400);
      }
    } catch ({ response, status }) {
      errorCode(response, status);
    }
  }

  async getWork(id: number) {
    try {
      let work = await this.prisma.work.findFirst({
        where: { id: +id },
        include: {
          work_type_detail: {
            include: { work_type: true },
          },
          user: true,
        },
      });
      if (work) {
        let { work_type_detail, user, ...result } = work;
        let { work_type, name } = work_type_detail;
        let data = {
          id: work.id,
          work: result,
          work_type_name: work_type.name,
          work_type_detail_name: name,
          user_name: user.name,
          avatar: user.avatar,
        };
        return successCode('Lấy công việc chi tiết thành công', data);
      } else {
        failCode('Không tìm thấy công việc chi tiết', 400);
      }
    } catch ({ response, status }) {
      errorCode(response, status);
    }
  }

  async getWorksByName(name: string) {
    try {
      let works = await this.prisma.work.findMany({
        where: {
          name: { contains: name },
        },
        include: {
          work_type_detail: {
            include: { work_type: true },
          },
          user: true,
        },
      });
      if (works.length > 0) {
        let data = works.map((work) => {
          let { work_type_detail, user, ...result } = work;
          let { work_type, name } = work_type_detail;
          return {
            id: work.id,
            work: result,
            work_type_name: work_type.name,
            work_type_detail_name: name,
            user_name: user.name,
            avatar: user.avatar,
          };
        });
        return successCode('Lấy danh sách công việc theo tên thành công', data);
      } else {
        failCode('Không tìm thấy danh sách công việc theo tên', 400);
      }
    } catch ({ response, status }) {
      errorCode(response, status);
    }
  }
}
