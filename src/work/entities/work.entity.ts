import { ApiProperty } from '@nestjs/swagger';

export class WorkViewModel {
  @ApiProperty({ name: 'id', type: Number })
  id: number;

  @ApiProperty({ name: 'name', type: String })
  name: string;

  @ApiProperty({ name: 'rate', type: Number })
  rate: number;

  @ApiProperty({ name: 'price', type: Number })
  price: number;

  @ApiProperty({ name: 'user_id', type: Number })
  user_id: number;

  @ApiProperty({ name: 'description', type: String })
  description: string;

  @ApiProperty({ name: 'work_type_detail_id', type: Number })
  work_type_detail_id: number;

  @ApiProperty({ name: 'short_desc', type: String })
  short_desc: string;

  @ApiProperty({ name: 'stars', type: Number })
  stars: number;
}
