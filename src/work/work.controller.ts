import {
  Body,
  Controller,
  Delete,
  Get,
  Headers,
  HttpCode,
  Param,
  Post,
  Put,
  Query,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { WorkService } from './work.service';
import {
  ApiBearerAuth,
  ApiBody,
  ApiConsumes,
  ApiHeaders,
  ApiParam,
  ApiQuery,
  ApiTags,
} from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { WorkViewModel } from './entities/work.entity';
import { FileUploadDto } from 'src/user/dto/user.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { SharpPipe } from 'src/config/sharp.pipe';

@ApiTags('Work')
@Controller('work')
export class WorkController {
  constructor(private readonly workService: WorkService) {}

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(200)
  @Get()
  getWorks() {
    return this.workService.getWorks();
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiHeaders([{ name: 'token' }])
  @ApiBody({ type: WorkViewModel })
  @HttpCode(200)
  @Post()
  createWork(@Headers('token') token: string, @Body() work: WorkViewModel) {
    return this.workService.createWork(token, work);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiQuery({ name: 'pageIndex', type: Number })
  @ApiQuery({ name: 'pageSize', type: Number })
  @ApiQuery({ name: 'keyword', type: String, required: false })
  @HttpCode(200)
  @Get('pagination')
  worksPagination(
    @Query('pageIndex') pageIndex: number,
    @Query('pageSize') pageSize: number,
    @Query('keyword') keyword: string,
  ) {
    return this.workService.worksPagination(pageIndex, pageSize, keyword);
  }

  // Lưu ý: getWorkTypesMenu phải đặt trước getWorkById vì nếu đặt sau thì trình biên dịch sẽ hiểu nhầm endpoint 'get-work-type-menu' là '/:id'
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(200)
  @Get('get-work-type-menu')
  getWorkTypesMenu() {
    return this.workService.getWorkTypesMenu();
  }

  // Đặt getWorkById sau getWorkTypesMenu
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiParam({ name: 'id', type: Number })
  @HttpCode(200)
  @Get('/:id')
  getWorkById(@Param('id') id: number) {
    return this.workService.getWorkById(id);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiHeaders([{ name: 'token' }])
  @ApiParam({ name: 'id', type: Number })
  @ApiBody({ type: WorkViewModel })
  @HttpCode(200)
  @Put('/:id')
  updateWork(
    @Headers('token') token: string,
    @Param('id') id: number,
    @Body() work: WorkViewModel,
  ) {
    return this.workService.updateWork(token, id, work);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiHeaders([{ name: 'token' }])
  @ApiParam({ name: 'id', type: Number })
  @HttpCode(200)
  @Delete('/:id')
  deleteWork(@Headers('token') token: string, @Param('id') id: number) {
    return this.workService.deleteWork(token, id);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiHeaders([{ name: 'token' }])
  @ApiParam({ name: 'WorkId', type: Number })
  @ApiConsumes('multipart/form-data')
  @ApiBody({ type: FileUploadDto })
  @UseInterceptors(FileInterceptor('file'))
  @HttpCode(200)
  @Post('upload-image/:WorkId')
  uploadWorkImage(
    @Headers('token') token: string,
    @Param('WorkId') id: number,
    @UploadedFile(SharpPipe) image: string, // nén hình bằng SharpPipe
  ) {
    return this.workService.uploadWorkImage(token, id, image);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiParam({ name: 'WorkTypeId', type: Number })
  @HttpCode(200)
  @Get('get-work-type-detail/:WorkTypeId')
  getWorkTypeDetails(@Param('WorkTypeId') id: number) {
    return this.workService.getWorkTypeDetails(id);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiParam({ name: 'WorkTypeDetailId', type: Number })
  @HttpCode(200)
  @Get('get-work-by-work-type-detail/:WorkTypeDetailId')
  getWorksByWorkTypeDetailId(@Param('WorkTypeDetailId') id: number) {
    return this.workService.getWorksByWorkTypeDetailId(id);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiParam({ name: 'WorkId', type: Number })
  @HttpCode(200)
  @Get('get-work/:WorkId')
  getWork(@Param('WorkId') id: number) {
    return this.workService.getWork(id);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @ApiParam({ name: 'WorkName', type: String })
  @HttpCode(200)
  @Get('get-work-by-name/:WorkName')
  getWorksByName(@Param('WorkName') name: string) {
    return this.workService.getWorksByName(name);
  }
}
