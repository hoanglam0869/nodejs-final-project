import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as express from 'express';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { ValidationPipe } from '@nestjs/common';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  // đảm bảo các endpoint không nhận dữ liệu không chính xác
  app.useGlobalPipes(new ValidationPipe());
  // đặt tiền tố chung
  app.setGlobalPrefix('api');
  // CORS
  app.enableCors();
  // định vị lại đường dẫn để load tài nguyên
  app.use(express.static('.'));

  // swagger
  // yarn add @nestjs/swagger swagger-ui-express
  const config = new DocumentBuilder()
    .setTitle('Fiverr')
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, config);
  // localhost:8080/swagger
  SwaggerModule.setup('/swagger', app, document);

  await app.listen(8082);
}
bootstrap();

// yarn start => node index.js
// yarn start:dev => nodemon index.js

// user => controller, module, service
// food => controller, module, service
// product => controller, module, service

// module => nest g module user
// controller => nest g controller user --no-spec
// service => nest g service user --no-spec

// auth
// nest g resource auth --no-spec

// prisma
// yarn add prisma @prisma/client
// yarn prisma init
// yarn prisma db pull
// yarn prisma generate

// bcrypt: mã hóa mật khẩu
// yarn add bcrypt @types/bcrypt

// jwt
// yarn add @nestjs/passport passport passport-local @nestjs/jwt passport-jwt @types/passport-jwt

// lấy dữ liệu từ biến môi trường
// yarn add @nestjs/config

// upload hình ảnh
// yarn add -D @types/multer

// nén hình ảnh
// yarn add sharp

// validation
// yarn add class-validator class-transformer
